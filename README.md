# ANTLR to JetBrains Editor


A maven plugin to generate a JetBrains editor for a given antlr grammar

## Editor Configuration

Some of the features of the editor can not be extracted from the grammar definition, e.g. syntax highlighting colors,
reference resolution, folding, etc. For this, a configuration file (yml format) can be provided. The configuration 
file accepts the following sections:

### Highlighting - Colors
A list of highlighting configurations. The following settings are accepted (only of of tokens, regex ot chars should be defined):

- **key** (requried): An id for the highlighter
- **color** (requried): The color to use, must be one of the *TextAttributesKey* in [DefaultLanguageHighlighterColors](https://github.com/JetBrains/intellij-community/blob/master/platform/editor-ui-api/src/com/intellij/openapi/editor/DefaultLanguageHighlighterColors.java)
- **tokens** : A list of the TOKENS that will use this highlight (per your grammar)
- **regex** : A Java regular expression to match a set of TOKENS.
- **chars** : A list of characters to highlight (e.g. for language structure indications -- if not defined as TOKENS).


If not provided, the generation will use the following default configuration:

```yaml
highlights:
  - key: ID
    color: DefaultLanguageHighlighterColors.IDENTIFIER
    tokens:
      - ID
  - key: STRING
    color: DefaultLanguageHighlighterColors.STRING
    tokens:
      - STRING
  - key: NUMBER
    color: DefaultLanguageHighlighterColors.NUMBER
    tokens:
      - INT
      - FLOAT
  - key: LINE_COMMENT
    color: DefaultLanguageHighlighterColors.LINE_COMMENT
    tokens:
      - LINE_COMMENT
  - key: BLOCK_COMMENT
    color: DefaultLanguageHighlighterColors.BLOCK_COMMENT
    tokens:
      - COMMENT
  - key: BRACES
    color: DefaultLanguageHighlighterColors.BRACES
    chars: ['(', ')', ':', ',', '[', ']', '{', '}']    
  - key: KEYWORD
    color: DefaultLanguageHighlighterColors.KEYWORD
    regex: '[a-zA-Z]+'   # Regex must be inside single quotes
  - key: OPERATOR
    color: DefaultLanguageHighlighterColors.OPERATION_SIGN
    regex: '[-!*/+<=>|&\.]+'
```

### Resolving references (TBD)
By default references are searched globally. That is, consider a simple grammar:

    script
    	:	vardef* function* statement* EOF
    	;
    function
    	:	'func' ID '(' formal_args? ')' (':' type)? block
    	;
    formal_args : formal_arg (',' formal_arg)* ;
        
    formal_arg : ID ;
    block
    	:  '{' (statement|vardef)* '}';
    	
    vardef : 'var' ID '=' expr ;
    
    statement
    	:	'if' '(' expr ')' statement ('else' statement)?		# If
    	...
    	|	ID '=' expr											# Assign
    ...
    
And the script:
    
    var n = 0;
    func foo(x) {
        return n*x;
    }

When resolving n, all *vardef* constructs are searched. In some languages it is usefull to limit the context of the search, i.e. global variables are not allowed and hence the nested *n* is an error -- variable not defined); support for context-based search is under research.


 
## Running the Editor
You need to have the [PluginDev Kit](https://www.jetbrains.org/intellij/sdk/docs/basics/getting_started/setting_up_environment.html) up and running in your IntelliJ installation. 

### Import the project as plugin
Make sure to reuse the iml file when prompted. You also need to have setup your IntelliJ for plugin development.
package tech.kinori.antlr.jetbrains.editor;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.antlr.parser.antlr4.ANTLRv4Parser.GrammarSpecContext;
import org.antlr.parser.antlr4.ANTLRv4Parser.GrammarTypeContext;
import org.antlr.v4.runtime.RuleContext;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.eclipse.epsilon.common.util.StringProperties;
import org.eclipse.epsilon.egl.EglFileGeneratingTemplateFactory;
import org.eclipse.epsilon.egl.EgxModule;
import org.eclipse.epsilon.emc.antlr.AntlrModel;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.exceptions.models.EolModelElementTypeNotFoundException;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;
import org.eclipse.epsilon.eol.execute.context.Variable;
import org.eclipse.epsilon.eol.types.EolCollectionType;
import org.eclipse.epsilon.eol.types.EolPrimitiveType;
import org.eclipse.epsilon.eol.types.EolType;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;


@Mojo( name = "createEditor")
public class JetbrainsEditorGenerator extends AbstractMojo {
	
	/** The path to the grammar definition, "*.antlr" */
    @Parameter(required = true)
    private String grammar;
    
	private Path grammarPath;
    
    /** The path to the grammar icon (16x16 px), "*.png" */
    @Parameter(required = true)
    private String icon;
    
	private Path iconPath;
    
    /** The extension used for grammar files." */
    @Parameter(defaultValue = "grammar")
	private String fileExtension;

    /** The base package for the editor classes */
    @Parameter(required = true)
	private String basePackage;
    
    /** The folder where the plugin project is generated" */ // FIXME Default to home/ ?
    @Parameter(required = true)
	private String outputFolder;
    
    /** The name of the editor, defaults to "[GrammarName] Editor" is used */
    @Parameter
	private String editorName;
    
    /** Editor plugin Description, defaults to "An IntelliJ editor for [GrammarName] files" */
    @Parameter
 	private String editorDesc;
    
    /** Company Info */
    @Parameter( defaultValue = "YourCompany")
	private String vendorName;
    @Parameter( defaultValue = "support@yourcompany.com")
	private String vendorEmail;
	@Parameter( defaultValue = "http://www.yourcompany.com")
	private String vendorUrl;
	
	/** Target multiple products */
	@Parameter
	private boolean targetAll;
	
	/** IntelliJ needs to know which tokens are COMMENTS and WHITE_SPACE */
	@Parameter
	private String[] commentTokens;
	@Parameter
	private String[] wsTokens;
	@Parameter
	private String[] stringTokens;
	
	/** Editor Configuration information */
	@Parameter
	private String editorConfig;

	private EgxModule eglModule;
	
	public void execute() throws MojoExecutionException, MojoFailureException {
		
		System.out.println(outputFolder);
		grammarPath = Paths.get(grammar);
		if (!Files.exists(grammarPath)) {
			throw new MojoExecutionException("Grammar file does not exist. File not found: " + grammarPath);
		}
		getLog().debug("grammarPath is " + grammarPath.toAbsolutePath());
		
		iconPath = Paths.get(icon);
		if (!Files.exists(iconPath)) {
			throw new MojoExecutionException("Icon file does not exist.");
		}
		getLog().debug("iconPath is " + iconPath.toAbsolutePath());
		
		// Load the grammar so we can start accessing information
		AntlrModel antlrModel = loadAntlrModel();
		
		String grammarName = getGrammarName(antlrModel);
		getLog().debug("grammarName is " + grammarName);
		String grammarPackage = grammarName.toLowerCase();
		String projectName = String.format("%s.%s.jetbrains", basePackage, grammarPackage);
		getLog().info(String.format("Project name will be: %s", projectName));
		
		// Create project structure
		Path projectPath = createProjectStructure(projectName);
		
		// Copy icon
		String iconName = copyIcon(grammarPackage, projectName);
		
		// Copy antlr runtime jar and java classes
		copyAntlrArtifacts(projectPath);


		// Copy *.iml file so antlr lib is loaded
		copyProjectSettings(grammarName, grammarPackage, projectPath);
		
		// Editor configuration
		Object config = getEditorConfiguration();
		if (!(config instanceof Map)) {
			throw new MojoExecutionException("Yaml configuration is malformed, root should be a map.");
		}
		
		eglModule = new EgxModule(new EglFileGeneratingTemplateFactory());
		
		// Parse the EGX runner
		Path editorEgxPath = null;
		try {
			URL editorEgxResource = getClass().getResource("templates/GrammarToEditor.egx");
			editorEgxPath = Paths.get(editorEgxResource.toURI());
		} catch (URISyntaxException e) {
			getLog().info("GrammarToEditor.egx should always be found. Broken jar?");
		}
		try {
			eglModule.parse(editorEgxPath.toFile());
		} catch (Exception e) {
			throw new MojoExecutionException("Error parsing EGX Template." + e.getMessage());
		}
		
		// Add model to module
		eglModule.getContext().getModelRepository().addModel(antlrModel);
				
		// Create params
		getLog().debug("Adding context variables.");
		createContextString("targetRoot", projectPath.toAbsolutePath().toString());
		createContextString("editorName", editorName);
		createContextString("editorDesc", editorDesc, String.format("An IntelliJ editor for %s files.", grammarName));
		createContextString("vendorName", vendorName);
		createContextString("vendorEmail", vendorEmail);
		createContextString("vendorUrl", vendorUrl);
		createContextBoolean("targetAll", targetAll);
		createContextString("basePackage", basePackage);
		createContextString("grammarName", grammarName);
		createContextString("grammarPackage", grammarPackage);
		Path metainfFolder = Paths.get(outputFolder, projectName, "resources/META-INF");
		createContextString("metaInfFolder", metainfFolder.toString());
		createContextString("icon", iconName);
		createContextString("fileExtension", fileExtension);
		createContextVariable("commentTokens", EolCollectionType.Sequence, Arrays.asList(commentTokens));
		createContextVariable("wsTokens", EolCollectionType.Sequence, Arrays.asList(wsTokens));
		createContextVariable("stringTokens", EolCollectionType.Sequence, Arrays.asList(stringTokens));
		
		// Add the highlights configuration information
		createHighlightVariable(config);
		// Refernce contributors
		createReferenceContributorsVariable(config);
		
		try {
			eglModule.execute();
		} catch (EolRuntimeException e) {
			throw new MojoExecutionException("Error generating files", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void createReferenceContributorsVariable(Object config) {
		getLog().info("Creating Reference Contributors context information.");
		Object rcs = ((Map<String, Object>)config).get("referenceContributors");
		if (rcs == null) {
			getLog().warn("No reference contributors information found in the editor configuration "
					+ "(key 'referenceContributors' not found).");
			rcs = Collections.emptyList();
		}
		createContextVariable("refContribs", EolCollectionType.Sequence, rcs);
	}

	/**
	 * Extract the highlight information from the configuration and pass it as a variable to the EGL execution.
	 * 
	 * @param config
	 * @throws MojoExecutionException 
	 */
	@SuppressWarnings("unchecked")
	private void createHighlightVariable(Object config) throws MojoExecutionException {
		getLog().info("Creating highlight context information.");
		Object hls = ((Map<String, Object>)config).get("highlights");
		if (hls == null) {
			throw new MojoExecutionException("No highlighting information found in the editor configuration "
					+ "(key 'highlights' not found).");
		}
		createContextVariable("highlights", EolCollectionType.Sequence, hls);
	}

	/**
	 * @return
	 * @throws MojoExecutionException
	 */
	private Object getEditorConfiguration() throws MojoExecutionException {
		getLog().info("Getting editor configuration.");
		Path sourcePath;
		sourcePath = editorConfig == null ? getResourcePath("/defaults.yml") : Paths.get(editorConfig);
		getLog().info(String.format("Using %s for editor configuration.", sourcePath));
		YamlReader reader;
		try {
			reader = new YamlReader(new FileReader(sourcePath.toString()));
		} catch (FileNotFoundException e1) {
			throw new MojoExecutionException("Error loading editor configuration at ." + sourcePath, e1);
		}
	    Object config;
		try {
			config = reader.read();
		} catch (YamlException e1) {
			throw new MojoExecutionException("Error reading editor configuration at ." + sourcePath, e1);
		}
		return config;
	}

	/**
	 * @param grammarName
	 * @param grammarPackage
	 * @param projectPath
	 * @throws MojoExecutionException
	 */
	private void copyProjectSettings(String grammarName, String grammarPackage, Path projectPath)
			throws MojoExecutionException {
		getLog().info("Copying project settings.");
		Path sourcePath;
		Path targetPath;
		editorName = editorName == null ? String.format("%s Editor", grammarName) : editorName;
		sourcePath = getResourcePath("/intellij.iml");
		targetPath = Paths.get(projectPath.toAbsolutePath().toString(),
				String.format("%s.%s.jetbrains.iml", basePackage, grammarPackage));
		copyPaths(sourcePath, targetPath);
		getLog().debug("copied iml file");
	}

	/**
	 * Copy the antlr runtime jar and adaptor classes
	 * 
	 * @param projectPath
	 * @throws MojoExecutionException
	 * @throws MojoFailureException
	 */
	private void copyAntlrArtifacts(Path projectPath) throws MojoExecutionException, MojoFailureException {
		getLog().info("Copying antlr artifacts.");
		Path sourcePath = null;
		String resource = "/antlr/antlr-runtime-4.7.1.jar";
		sourcePath = getResourcePath(resource);
		Path targetPath = Paths.get(projectPath.toAbsolutePath().toString(), "lib", sourcePath.getFileName().toString());
		copyPaths(sourcePath, targetPath);
		getLog().debug("copied antlr jar");
		String[] javaFiles = {"/antlr/lexer/ANTLRLexerState.java", "/antlr/lexer/CharSequenceCharStream.java",
				"/antlr/parser/ErrorStrategyAdaptor.java", "/antlr/parser/SyntaxError.java",
				"/antlr/parser/SyntaxErrorListener.java"};
		for (int i=0; i<javaFiles.length;i++) {
			sourcePath = getResourcePath(javaFiles[i]);
			targetPath = Paths.get(projectPath.toAbsolutePath().toString(),
					"src/org/antlr/jetbrains/adaptor",
					sourcePath.toString().contains("parser") ? "parser" : "lexer",
					sourcePath.getFileName().toString());
			copyPaths(sourcePath, targetPath);
		}
		getLog().debug("antlr artifacts copied.");
	}

	/**
	 * @param grammarPackage
	 * @param projectName
	 * @return
	 * @throws MojoFailureException
	 */
	private String copyIcon(String grammarPackage, String projectName) throws MojoExecutionException {
		getLog().info("Copying icon: " + iconPath);
		String iconName = iconPath.getFileName().toString();
		Path target = Paths.get(outputFolder, projectName, "src");
		Path targetIconPath = getPackagePath(target, basePackage);
		targetIconPath = targetIconPath.resolve(Paths.get(grammarPackage, "icons", iconName));
		copyPaths(iconPath, targetIconPath);
		getLog().debug("copied icon");
		return iconName;
	}

	/**
	 * @param projectName
	 * @return
	 * @throws MojoExecutionException
	 */
	private Path createProjectStructure(String projectName) throws MojoExecutionException {
		getLog().info("Creating project structure");
		Path dir = Paths.get(outputFolder, projectName);
		Path projectPath = null;
		try {
			projectPath = Files.createDirectories(dir);
		} catch (FileAlreadyExistsException fe) {
			getLog().info("Project folder already exists.");
			projectPath = dir;
		} catch (IOException e) {
			throw new MojoExecutionException("Error creating project folder.", e);
		}
		getLog().info("project dir is " + projectPath.toAbsolutePath().toString());
		
		String[] folders = {"gen", "src", "resources", "resources/META-INF", "lib",
				"src/org", "src/org/antlr", "src/org/antlr/jetbrains", "src/org/antlr/jetbrains/adaptor",
				"src/org/antlr/jetbrains/adaptor/lexer", "src/org/antlr/jetbrains/adaptor/parser"};
		for (int i=0;i<folders.length;i++) {
			dir = Paths.get(outputFolder, projectName, folders[i]);
			try {
				Files.createDirectory(dir);
			} catch (FileAlreadyExistsException fe) {
				getLog().info(String.format("Project folder (%s) already exists.", folders[i]));
			} catch (IOException e) {
				throw new MojoExecutionException("Error creating project subfolders. " + e.getMessage());
			}
		}
		getLog().info("generated folder structure for project");
		return projectPath;
	}

	/**
	 * @param antlrModel
	 * @return
	 * @throws MojoExecutionException
	 */
	private String getGrammarName(AntlrModel antlrModel) throws MojoExecutionException {
		String grammarName;
		try {
			Collection<RuleContext> grammarSpecs = antlrModel.getAllOfType("grammarSpec");
			if (grammarSpecs.isEmpty()) {
				throw new MojoExecutionException("Failed to find GrammarSpec. ");
			}
			GrammarSpecContext grammarSpec = (GrammarSpecContext) grammarSpecs.iterator().next();
			
			GrammarTypeContext grammarType = grammarSpec.grammarType();
			// Make sure grammar is parse, not lexer
			if (grammarType.LEXER() != null) {
				throw new MojoExecutionException("Grammar must be a paser grammar or plain grammar. Provided grammar was a lexer grammar.");
			}
			grammarName = grammarSpec.identifier().TOKEN_REF().getText();
		} catch (EolModelElementTypeNotFoundException e) {
			throw new MojoExecutionException("Failed to find GrammarSpec. " + e.getMessage());
		}
		return grammarName;
	}

	/**
	 * @return
	 * @throws MojoExecutionException
	 */
	private AntlrModel loadAntlrModel() throws MojoExecutionException {
		AntlrModel antlrModel = new AntlrModel();
		
		StringProperties properties = new StringProperties();
		properties.put(AntlrModel.PROPERTY_PATH, grammarPath.toAbsolutePath());
		try {
			antlrModel.load(properties);
		} catch (EolModelLoadingException e) {
			throw new MojoExecutionException("Failed to load the grammar. " + e.getMessage());
		}
		return antlrModel;
	}

	/**
	 * @param resourcePath
	 * @return
	 * @throws MojoExecutionException If the resource is not found
	 */
	protected Path getResourcePath(String resourcePath) throws MojoExecutionException {
		Path sourcePath = null;
		try {
			URL resourceUrl = getClass().getResource(resourcePath);
			sourcePath = Paths.get(resourceUrl.toURI());
		} catch (NullPointerException e) {
			getLog().info("Resources should always be found. Broken jar?");
			throw new MojoExecutionException("Error getting resource. " + resourcePath, e);
		} catch (URISyntaxException e1) {
			getLog().info("Shuold never get here!");
		}
		return sourcePath;
	}
	
	/**
	 * Copy the file specified by the source path to the file specified by the the target path.
	 * @param sourcePath
	 * @param targetPath
	 * @throws MojoFailureException
	 */
	private void copyPaths(Path sourcePath, Path targetPath) throws MojoExecutionException {
		try {
			Files.copy(sourcePath, targetPath);
		} catch(FileAlreadyExistsException e) {
		    getLog().info("Path " + targetPath + " already exists, skipping");
		} catch (IOException e1) {
			// Missing directories
			try {
				getLog().info("Missing directories in " + targetPath + ". Craetig them.");
				Path subpath = targetPath.getRoot().resolve(targetPath.subpath(0, targetPath.getNameCount()-1));
				Files.createDirectories(subpath);
			} catch (IOException e) {
				throw new MojoExecutionException("Error copying icon file. " + e.getMessage());
			}
			try {
				Files.copy(iconPath, targetPath);
			} catch (IOException e) {
				throw new MojoExecutionException("Error copying icon file. " + e.getMessage());
			}
		}
	}
	
	/**
	 * Transform a qualified package name (dot separated) to a path, nested below the target path
	 * @param outputPath
	 * @param targetPackage
	 * @return
	 */
	private Path getPackagePath(Path outputPath, String targetPackage) {
		//Path path = Paths.get(outputFolder, projectName, "src");
		Path path = outputPath;
		String[] packageFolders = targetPackage.split("\\.");
		for (int i=0;i<packageFolders.length;i++) {
			path = path.resolve(packageFolders[i]);
		}
		return path;
	}
	
	private void createContextString(String name, String value) {
		createContextVariable(name, EolPrimitiveType.String, value);
	}
	
	private void createContextString(String name, String value, Object ifNull) {
		createContextVariable(name, EolPrimitiveType.String, value, ifNull);
	}
	
	private void createContextBoolean(String name, boolean value) {
		createContextVariable(name, EolPrimitiveType.Boolean, value);
	}
	private void createContextVariable(String name, EolType type, Object value) {
		createContextVariable(name, type, value, null);
	}
	
	private void createContextVariable(String name, EolType type, Object value, Object ifNull) {
		Variable var = new Variable(name, type);
		if (ifNull == null) {
			var.setValueBruteForce(value);
		}
		else {
			var.setValueBruteForce(editorDesc == null ? ifNull : editorDesc);
		}
		eglModule.getContext().getFrameStack().put(var);
	}

}

package tech.kinori.antlr.jetbrains.editor;

import java.io.File;

import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.junit.Test;

public class GeneratorTest extends AbstractMojoTestCase {
	
	/** {@inheritDoc} */
    protected void setUp() throws Exception {
        super.setUp();
    }

    /** {@inheritDoc} */
    protected void tearDown() throws Exception {
        super.tearDown();
    }
	
//	@Rule
//    public MojoRule mojoRule = new MojoRule();
	
	@Test
	public void testGenerator() throws Exception {
		
		
		File pom = getTestFile( "src/test/resources/unit/project-to-test/pom.xml" );
//		File pom = new File("src/test/resources/unit/project-to-test");
		assertNotNull( pom );
		assertTrue( pom.exists() );

        //JetbrainsEditorGenerator generator = (JetbrainsEditorGenerator) mojoRule.lookupConfiguredMojo(pom, "createEditor");
		JetbrainsEditorGenerator generator = (JetbrainsEditorGenerator) lookupMojo("createEditor", pom);
        assertNotNull( generator );
        generator.execute();
        
        
//		JetbrainsEditorGenerator generator = new JetbrainsEditorGenerator();
//		//String property = System.getProperty("grammarpath");
//		String grammar = "D:/Users/Goblin/epsilonws/org.eclipse.epsilon.emc.antlr.test/src/org/eclipse/epsilon/emc/antlr/test/SampleLanguage.g4";
//		String icon = "D:/Users/Goblin/epsilonws/org.eclipse.epsilon.emc.antlr.test/src/org/eclipse/epsilon/emc/antlr/test/sample.png";
//		Path grammarPath = Paths.get(grammar);
//		generator.setGrammarPath(grammarPath );
//		generator.setIconPath(Paths.get(icon));
//		generator.setFileExtension("sample");
//		generator.execute();
		
	}

}
